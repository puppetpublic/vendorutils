#
# Installs HP and Dell management utilities.
# See https://ikiwiki.stanford.edu/software/vendorutils
#
class vendorutils {

  # is_virtual is not a boolean!
  if ($::is_virtual == 'true') {
    fail ('Class vendorutils not supported on virtual machines.')
  } else {
    case $::manufacturer {
      # HP currently unimplmented
      #'HP': {
      #  case $::productname {
      #    'ProLiant DL165 G7': { package { 'hp-health': ensure => absent } }
      #    'ProLiant DL180 G6', 'ProLiant DL380 G5', 'ProLiant DL380 G7': { package { 'hp-health': ensure => installed } }
      #  }

      #  package { 'ss-scripting-toolkit':
      #    ensure => installed,
      #  }
      #}
      /^Dell\b/: {
        case $::productname {
          'PowerEdge 1850', 'Optiplex 990', 'VMware Virtual Platform': {
              fail ("Vendorutils not supported on ${::productname}")
            }
          default: {
            case $::operatingsystem {
              'RedHat': {
                case $::lsbmajdistrelease {
                  '4': { # not available for RHEL4
                    fail ("Vendorutils not supported on ${::operatingsystem} ${::lsbmajdistrelease}")
                  }
                  '5': { include vendorutils::openmanage }
                  # compat-libstdc++-33
                  default: {
                    case $::architecture {
                      'x86_64': { include vendorutils::openmanage }
                      default: {  # not available for 32bit RHEL6+
                        fail ("Vendorutils not supported on ${::operatingsystem} ${::lsbmajdistrelease} ${::architecture}")
                      }
                    }
                  }
                }
              }
              'Debian': {
                case $::lsbdistcodename {
                  'wheezy': { # not available -- Jason is working on wheezy
                    fail ("Vendorutils not supported on ${::operatingsystem} ${::lsbmajdistcodename}")
                  }
                  default: { include vendorutils::openmanage }
                }
              }
              default: { include vendorutils::openmanage }
            }
          }
        }
      }
      default: {
        fail ("Vendorutils not supported on ${::manufacturer}")
      }
    }
  }
}
