# class that actually installs and configures vendorutils
class vendorutils::openmanage inherits vendorutils {
  package {
    [
      'srvadmin-base',
      'srvadmin-deng',
      'srvadmin-idrac',
      'srvadmin-rac4',
      'srvadmin-rac5',
      'srvadmin-storageservices',
    ]:
      ensure => installed;
  }

  file { '/opt/dell/srvadmin/var/lib/srvadmin-deng/dcsnmp.off':
    ensure  => file,
    mode    => '0444',
    owner   => 'root',
    group   => 'root',
    require => Package['srvadmin-deng'];
  }

  file { '/etc/filter-syslog/openmanage':
    source => 'puppet:///modules/vendorutils/etc/filter-syslog/openmanage',
  }

  exec { 'srvadmin-services':
    path        =>
      '/usr/sbin:/usr/bin:/sbin:/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin',
    command     => '/opt/dell/srvadmin/sbin/srvadmin-services.sh restart',
    refreshonly => true,
    require     => File['/opt/dell/srvadmin/var/lib/srvadmin-deng/dcsnmp.off'],
    subscribe   => Package['srvadmin-deng'];
  }
}
