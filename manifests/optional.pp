#
# Class to determine if the system satisfies the criteria for vendorutils
# See https://ikiwiki.stanford.edu/software/vendorutils
#
class vendorutils::optional {
  # is_virtual is not a boolean!
  if (
    ($::is_virtual != 'true') and
    ($::manufacturer =~ /^Dell\b/ and
      !($::productname in [ 'PowerEdge 1850', 'Optiplex 990', 'VMware Virtual Platform' ]) and
      (
        ($::operatingsystem == 'Redhat' and
          ($::lsbmajdistrelease == 5 or
            ($::lsbmajdistrelease > 5 and $::architecture == 'x86_64')
          )
        ) or
        ($::operatingsystem == 'Debian' and $::lsbmajdistrelease <= 6)
      )
    )
  ) {include vendorutils}
}
